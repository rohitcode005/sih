package com.hackathon.sih

import android.content.Intent
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import android.webkit.WebViewClient
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class pop_up_otp_verification : AppCompatActivity() {
    private val verify_opt_btn: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pop_up_otp_verification)

        val intent = intent

        val dm = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(dm)

        val width = dm.widthPixels
        val height = dm.heightPixels

        window.setLayout((width * .95).toInt(), (height * .30).toInt())

        val verify_otp = findViewById<Button>(R.id.verify_otp);
        verify_otp.setOnClickListener(View.OnClickListener {
            val intent = Intent(this@pop_up_otp_verification, Dashboard::class.java)
            startActivity(intent)
        })

    }
}